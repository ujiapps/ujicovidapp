
# **ujicovidapp**

Aplicación en Dart/Flutter como ejemplo de desarrollo simple de sistema de rastreo y alerta por Covid.

## * Características de la aplicación

*   Rastreo de contactos por Bluetooth sin posicionamiento GPS o Wi-Fi.
*   Emisión y recepción de códigos aleatorios de forma anónima.
*   Anuncio de positivo al sistema tras diagnóstico.
*   Comprobación periódica de posibles contactos positivos.
*   Aviso de posible contacto con persona positiva.

## * Pantallas principales recomendadas [ToDo]

*   Pantallas deslizantes introductorias e informativas (1ª vez).
*   Pantalla principal:
    *   Mensaje del estado de la monitorización (ACTIVO o PAUSADO).
    *   Botón de activar/desactivar.
    *   Botón a pantalla de introducción de código de positivo.
*   Pantalla de introducción de código de positivo:
    *   Texto y cuadro para introducir el código.
    *   Ventana de información tras positivo.
*   Pantalla de aviso de contacto con contagiado.

## * Pantallas de test de funciones implementadas

*   Background/Foreground
*   Tx/Rx
*   Códigos
*   Visor de logs internos

## * Screenshots:

*   01_Home.jpg - Menú principal.
*   02_TxRxPermisos.jpg - Solicitud permisos para Tx/Rx.
*   03_AdminLogs.jpg - Taréas Admin.
*   04_CodesForm.jpg - Introducción código por positivo.
*   05_CodesAviso.jpg - Avisos por error en código.
*   06_CodesInfo.jpg - Confirmación código correcto.
*   07_TxRxVacio.jpg - Test Tx/Rx inicial.
*   08_TxRxDatos.jpg - Test Tx/Rx con datos.

## * Estado actual:

*   Emisión
*   Recepción
*   Generación códigos
*   Cálculo de distancia y potencia de señal
*   Algoritmo detección y contacto
*   Guardado en DB de códigos emitidos y recibidos
*   API codigo positivo
*   Formulario envío código por positivo
*   API envío códigos generados
*   Envío de códigos generados desde la App
*   API chequeo
*   [ToDo] Background (Ampliar más allá del tiempo de vida actual)
*   [ToDo] Función de chequeo en la app (por determinar momento e intervalo de consulta)
*   [ToDo] Diseño usable por usuario final

## * Observaciones de diseño:

*   **Aleatoriedad:**
    *   La aleatoriedad de los códigos viene dada por la combinación de los campos major y minor, con un valor entre 1 y 65.535 cada uno, que dan 4.200 millones de combinaciones posibles sin ninguna vinculación que posibilite identificar al dispositivo.
    *   El tiempo recomendado de renovación de los códigos es entre 1 y 8 horas.
*   **Algoritmo detección:**
    *   Los algoritmos detectado se almacenan en dos tablas. En primera instancia como contacto esporádico y, si procede, pasa a la segunda como contacto de riesgo.
    *   El paso de una tabla a otra se decide si se cumple el siguiente criterio: X detecciones dentro de un tiempo máximo Y con una distancia/intensidad Z. (DBProvider.checkRx())
*   **Códigos por positivo:**
    *   Es necesario introducir previamente en la tabla "lockcode" del servidor los códigos que se entregarán a los usuarios confirmados como positivos.
    *   Al introducir el código dado en la app, se comprueba en el servidor que el código es válido, y si lo es, la app envía todos los códigos generados en los ultimos días.
*   **Chequeo códigos:**
    *   Queda por definir la función de comprobación de códigos positivos, tanto el momento como la periodicidad de la misma (al arranque, cada X minutos/horas...) y el lado de proceso (envío de códigos generados por la app al servidor y él comprueba o envío de positivos del servidor a la app y ella chequea con sus recibidos).

## * Rendimiento:

*   Entre el 5% y el 10% del uso total de la batería.
*   Fluidez adecuada aún con horas de uso.
*   Sin problemas aparentes en el envío y recepción de las tramas iBeacon.
*   Precisión de los datos en los posibles contactos de riesgo muy configurables.
*   Cambio de librerías Background a Foreground: Mayor permanencia de la app con notificación en primer plano: 1-5 horas.
*   Versión SDK mínima: 23\. Librerías compatibles con el 84,9% de los dispositivos.

## * Compatibilidad Radar Covid:

*   La aplicación oficial no interfiere en la propia de la UJI. Al revés no se ha podido comprobar.

## * ToDo:

*   Desarrollo inicial completado a falta de testeos y ensamblaje final con diseño.
*   Compatibilidad de librerías para un posible versión de iOS.

## * Problemas:

*   Mejora con el paso de permanencia de la App en Background a Foreground, pero sin fiabilidad de permanencia a largo plazo.

## * Opciones de mejora:

*   Programación nativa de determinadas funciones.

## * Configuración inicial:

*   SQL: Ejecutar el script de creación de la base de datos MySQL. (/php/server.sql)
*   PHP: Modificar las variables conforme al entorno del servidor. (/php/conf.php)
*   APP: Modificar los parámetros conforme a las necesidades del despliegue. (/lib/src/lib.dart)
