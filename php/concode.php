<?php  

// API de envío de códigos positivos a la App.

require 'conf.php';
 
if(($_SERVER['REQUEST_METHOD'] == "POST")) { //  && (isset($_POST['appid'])) && (isset($_POST['lockcode']))
	
	$jsonPOST = file_get_contents('php://input');
	//$jsonIN = json_decode(stripslashes($_POST['req']));
	$jsonIN = json_decode($jsonPOST);
	//var_dump($_POST);
	//print_r($_POST);
	$appid = $jsonIN->{"appid"};
	$appid = mysqli_real_escape_string($con,$appid);
	$appid = strtoupper(preg_replace("/[^a-zA-Z0-9_-]/", '', $appid));
	$concode = $jsonIN->{"concode"};
	$concode = mysqli_real_escape_string($con,$concode);
	$concode = strtoupper(preg_replace("/[^a-zA-Z0-9_-]/", '', $concode));
    $sqlInsLog = "INSERT INTO log (logtxt) ";
	$sqlInsLog = $sqlInsLog." VALUES('PosCodeAPI:".$appid."#".$concode."')";
    $resInsLog = mysqli_query($con, $sqlInsLog) or die(mysqli_error());
    if($appid==$appidkey){
		$sqlConCode = "SELECT * FROM gencode WHERE gentime>DATE_SUB(now(), INTERVAL 15 DAY) AND (majorid minorid) ";
		$gencode_array = explode("_", $gencode);
		foreach ($gencode_array as $gencodemm) {
			$gencodeid = explode("-", $gencodemm);
			$sqlInsGen = $sqlInsGen."(".$rowLockCode['idlockcode'].",".$gencodeid[0].",".$gencodeid[1].",0),";
		}
		$sqlInsGen = $sqlInsGen."(0,0,0,0);";
		$resInsGen = mysqli_query($con, $sqlInsGen) or die(mysqli_error());

		$sqlUpdConf = "UPDATE lockcode SET send='1', sendtime=now() WHERE lockcode='".$lockcode."' ";
		$resUpdConf = mysqli_query($con, $sqlUpdConf) or die(mysqli_error());
		$json = array("res" => 0, "msg" => iconv($inchar, $outchar, "GenCode recibidos."));
    }else{
        $json = array("res" => 1, "msg" => iconv($inchar, $outchar, "AppId no valido."));
    }
}else{
	$json = array("res" => 1, "msg" => iconv($inchar, $outchar, "Metodo no aceptado."));
}

mysqli_close($con);



/* Output header */

header ('Content-type: text/html; charset=utf-8');
echo json_encode($json, JSON_UNESCAPED_UNICODE);
//echo json_encode($eventosdata_array, JSON_UNESCAPED_UNICODE);

//header('Content-Type: application/json; charset=ISO-8859-1');
//$json = array_map('htmlentities',$json);
//$json = html_entity_decode(json_encode($json));
//header('Content-type: application/json');
//echo $json;
exit;

?>