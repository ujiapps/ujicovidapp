<?php

// INI configuración PHP

$GLOBALS["AppName"]="UJI Covid App";

$appidkey=$_ENV["APPIDKEY"];  // Clave única para la comunicación con la API
$condbsrv=$_ENV["DBSERVER"];  // IP o nombre servidor DB
$condbusr=$_ENV["DBUSER"];    // Usuario DB
$condbpss=$_ENV["DBPASS"];   // Contraseña DB
$condbdbn=$_ENV["DBNAME"];   // Nombre DB

// FIN configuración PHP

$con =  mysqli_connect($condbsrv, $condbusr, $condbpss, $condbdbn);
$con->set_charset("utf8");
if (!$con) {
    die('Error de Conexion (' . mysqli_connect_errno() . ') '. mysqli_connect_error());
}
//$pagelog = 'Conectado a ' . mysqli_get_host_info($con) . "...";
$pagelog = date("Y-m-d H:i:s").'...Iniciando...';

$charenc = "UTF-8"; // También ISO-8859-1

// Posibles conversiones de charset
//utf8_encode = iconv("UTF-8", "ISO-8859-1", $text)
//utf8_decode = iconv("ISO-8859-1", "UTF-8", $text)
$inchar="UTF-8";
//$outchar="ISO-8859-1";
$outchar="UTF-8";

// Configuración Local
setlocale(LC_TIME, "es_ES.UTF8");
date_default_timezone_set('Europe/Madrid');
$fechanow = time();

?>
