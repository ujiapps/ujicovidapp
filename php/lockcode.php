<?php  

// API de comprobación de código por positivo

require 'conf.php';
 
if(($_SERVER['REQUEST_METHOD'] == "POST")) {

	//$json=$_GET ['json'];
	//$json=$_POST ['Accept'];
	$jsonPOST = file_get_contents('php://input');
	//$jsonIN = json_decode(stripslashes($_POST['req']));
	$jsonIN = json_decode($jsonPOST);
	//var_dump($_POST); // array(0) { }
	$appid = $jsonIN->{"appid"}; 
	$appid = mysqli_real_escape_string($con,$appid);
	$appid = strtoupper(preg_replace("/[^a-zA-Z0-9_-]/", '', $appid));
    $lockcode = $jsonIN->{"lockcode"}; 
	$lockcode = mysqli_real_escape_string($con,$lockcode);
	$lockcode = strtoupper(preg_replace("/[^a-zA-Z0-9_-]/", '', $lockcode));

    $sqlInsLog = "INSERT INTO log (logtxt) ";
	$sqlInsLog = $sqlInsLog." VALUES('LockCodeAPI:".$appid."#".$lockcode."')";
    $resInsLog = mysqli_query($con, $sqlInsLog) or die(mysqli_error());
    if($appid==$appidkey){
		
        $sqlLockCode = "SELECT * FROM lockcode WHERE lockcode='".$lockcode."' LIMIT 1";
		$resLockCode = mysqli_query($con, $sqlLockCode) or die(mysqli_error());
		//echo "ECHO";
		if(mysqli_num_rows($resLockCode) == 1) {
            $rowLockCode = mysqli_fetch_array($resLockCode);
            if($rowLockCode['conf'] == 0) {
                $sqlUpdConf = "UPDATE lockcode SET conf='1', conftime=now() WHERE lockcode='".$lockcode."' ";
                $resUpdConf = mysqli_query($con, $sqlUpdConf) or die(mysqli_error());
                $json = array("res" => 0, "msg" => iconv($inchar, $outchar, "LockCode validado."));
            }else{
                $json = array("res" => 1, "msg" => iconv($inchar, $outchar, "LockCode (".$lockcode.") ya utilizado."));
            }
        }else{
            $json = array("res" => 1, "msg" => iconv($inchar, $outchar, "LockCode (".$lockcode.") no valido."));
        }
    }else{
        $json = array("res" => 1, "msg" => iconv($inchar, $outchar, "AppId no valido."));
    }
}else{
	$json = array("res" => 1, "msg" => iconv($inchar, $outchar, "Metodo no aceptado."));
}

mysqli_close($con);



/* Output header */

header ('Content-type: text/html; charset=utf-8');
echo json_encode($json, JSON_UNESCAPED_UNICODE);
//echo json_encode($eventosdata_array, JSON_UNESCAPED_UNICODE);

//header('Content-Type: application/json; charset=ISO-8859-1');
//$json = array_map('htmlentities',$json);
//$json = html_entity_decode(json_encode($json));
//header('Content-type: application/json');
//echo $json;
exit;

?>