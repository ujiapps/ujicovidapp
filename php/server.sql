
/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Dumping database structure for ujidb
CREATE DATABASE IF NOT EXISTS `ujidb` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `ujidb`;

-- Dumping structure for table ujidb.gencode
CREATE TABLE IF NOT EXISTS `gencode` (
  `idgencode` int(11) NOT NULL AUTO_INCREMENT,
  `idlockcode` int(11) DEFAULT '0',
  `majorid` int(11) DEFAULT '0',
  `minorid` int(11) DEFAULT '0',
  `gentime` datetime DEFAULT NULL,
  `tsc` datetime DEFAULT CURRENT_TIMESTAMP,
  `tsu` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`idgencode`) USING BTREE,
  KEY `idreccode` (`idgencode`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- Data exporting was unselected.

-- Dumping structure for table ujidb.lockcode
CREATE TABLE IF NOT EXISTS `lockcode` (
  `idlockcode` int(11) NOT NULL AUTO_INCREMENT,
  `lockcode` varchar(16) DEFAULT NULL,
  `conf` int(1) DEFAULT '0',
  `conftime` datetime DEFAULT NULL,
  `send` int(1) DEFAULT '0',
  `sendtime` datetime DEFAULT NULL,
  `tsc` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `tsu` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`idlockcode`) USING BTREE,
  KEY `idposcode` (`idlockcode`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- Data exporting was unselected.

-- Dumping structure for table ujidb.log
CREATE TABLE IF NOT EXISTS `log` (
  `idlog` int(11) NOT NULL AUTO_INCREMENT,
  `logtxt` text,
  `ts` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`idlog`),
  KEY `idlog` (`idlog`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- Data exporting was unselected.

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
