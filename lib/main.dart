import 'package:flutter/material.dart';

//import 'package:ujicovidapp/src/pages/foreground_page.dart';
import 'package:ujicovidapp/src/pages/foreplug_page.dart';
//import 'package:ujicovidapp/src/pages/cast_page.dart';
//import 'package:ujicovidapp/src/pages/beacon_page.dart';
import 'package:ujicovidapp/src/pages/beaconcast_page.dart';
import 'package:ujicovidapp/src/pages/admin_page.dart';
import 'package:ujicovidapp/src/pages/codes_page.dart';

import 'package:ujicovidapp/src/services/sharedpref_serv.dart';

import 'package:ujicovidapp/src/pages/home_page.dart';

final colorBackground = const Color(0xFFF3F4F7);
final colorPrimary = const Color(0xFF35465B);
final colorAccent = const Color(0xFF7576FD);
final colorGrey = const Color(0xFFA5ADB7);

//void main() => runApp(MyApp());

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  final prefs = new SharedPrefServ();
  await prefs.initPrefs();
  runApp(MyApp());
  //maybeStartFGS();
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'UJI Covid App',
      theme: new ThemeData(
        brightness: Brightness.light,
        backgroundColor: colorBackground,
        primaryColor: colorPrimary,
        accentColor: colorAccent,
        splashColor: colorAccent,
        disabledColor: colorGrey,
      ),
      initialRoute: '/',
      routes: {
        '/': (BuildContext context) => HomePage(),
        //'/foreserv': (BuildContext context) => ForegroundApp(), // Servicio Foreground con foreground_service
        '/foreplug': (BuildContext context) =>
            ForePlugApp(), // Servicio Foreground con flutter_foreground_plugin
        //'/cast': (BuildContext context) => CastApp(), // Emisión beacon aislada
        //'/beacon': (BuildContext context) => BeaconApp(), // Lectura beacon aislada
        '/beaconcast': (BuildContext context) =>
            BeaconCastApp(), // Emisión y lectura conjuntas con registro en BD
        '/admin': (BuildContext context) =>
            AdminApp(), // Pantalla para chequeo de variables y tablas
        '/codes': (BuildContext context) =>
            CodesApp(), // Pantalla de envío de código por positivo
      },
    );
  }
}
