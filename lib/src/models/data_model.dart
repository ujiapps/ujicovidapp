class DataTx {
  DataTx({
    this.id,
    this.major,
    this.minor,
    this.timer,
  });

  int id;
  int major;
  int minor;
  int timer;

  DataTx copyWith({
    int id,
    int major,
    int minor,
    int timer,
  }) =>
      DataTx(
        id: id ?? this.id,
        major: major ?? this.major,
        minor: minor ?? this.minor,
        timer: timer ?? this.timer,
      );

  factory DataTx.fromJson(Map<String, dynamic> json) => DataTx(
        id: json["id"],
        major: json["major"],
        minor: json["minor"],
        timer: json["timer"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "major": major,
        "minor": minor,
        "timer": timer,
      };
}

class DataRx {
  DataRx({
    this.id,
    this.major,
    this.minor,
    this.timer,
    this.retry,
    this.accur,
    this.rssi,
  });

  int id;
  int major;
  int minor;
  int timer;
  int retry;
  double accur;
  int rssi;

  DataRx copyWith({
    int id,
    int major,
    int minor,
    int timer,
    int retry,
    double accur,
    int rssi,
  }) =>
      DataRx(
        id: id ?? this.id,
        major: major ?? this.major,
        minor: minor ?? this.minor,
        timer: timer ?? this.timer,
        retry: retry ?? this.retry,
        accur: accur ?? this.accur,
        rssi: rssi ?? this.rssi,
      );

  factory DataRx.fromJson(Map<String, dynamic> json) => DataRx(
        id: json["id"],
        major: json["major"],
        minor: json["minor"],
        timer: json["timer"],
        retry: json["retry"],
        accur: json["accur"].toDouble(),
        rssi: json["rssi"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "major": major,
        "minor": minor,
        "timer": timer,
        "retry": retry,
        "accur": accur,
        "rssi": rssi,
      };
}
