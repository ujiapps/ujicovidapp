// Librería que substituye a foreground_service

import 'package:flutter/material.dart';
//import 'dart:async';

import 'package:flutter_foreground_plugin/flutter_foreground_plugin.dart';

//import 'package:beacon_broadcast/beacon_broadcast.dart';

//import 'package:flutter/services.dart';
//import 'package:flutter_beacon/flutter_beacon.dart';

//void main() => runApp(MyApp());

class ForePlugApp extends StatefulWidget {
  @override
  _ForePlugAppState createState() => _ForePlugAppState();
}

class _ForePlugAppState extends State<ForePlugApp> {
  //String _platformVersion = 'Unknown';

  @override
  void initState() {
    startForegroundService();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Foreground Service for UJICovidApp'),
        ),
        body: Center(
          child: Column(
            children: <Widget>[
              RaisedButton(
                child: Text("START"),
                onPressed: () {
                  startForegroundService();
                },
              ),
              RaisedButton(
                child: Text("STOP"),
                onPressed: () async {
                  await FlutterForegroundPlugin.stopForegroundService();
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}

void startForegroundService() async {
  await FlutterForegroundPlugin.setServiceMethodInterval(seconds: 5);
  await FlutterForegroundPlugin.setServiceMethod(globalForegroundService);
  await FlutterForegroundPlugin.startForegroundService(
    holdWakeLock: false,
    onStarted: () {
      print("Foreground on Started");
    },
    onStopped: () {
      print("Foreground on Stopped");
    },
    title: "UJI Covid App",
    content: "Ejecutando...",
    iconName: "ic_stat_hot_tub.png",
  );
}

void globalForegroundService() async {
  debugPrint("Datetime actual ${DateTime.now()}");
  print("\nglobalForegroundService EJECUTADO");

  // Prueba para emitir y leer las tramas beacon desde el servicio.

  /*
  try {
  // if you want to manage manual checking about the required permissions
  await flutterBeacon.initializeScanning;
  
  // or if you want to include automatic checking permission
  await flutterBeacon.initializeAndCheckScanning;
} on PlatformException catch(e) {
  // library failed to initialize, check code and message
}

final regions = <Region>[];

if (Platform.isIOS) {
  // iOS platform, at least set identifier and proximityUUID for region scanning
  regions.add(Region(
      identifier: 'Apple Airlocate',
      proximityUUID: 'E2C56DB5-DFFB-48D2-B060-D0F5A71096E0'));
} else {
  // android platform, it can ranging out of beacon that filter all of Proximity UUID
  regions.add(Region(identifier: 'com.beacon'));
}

// to start ranging beacons
_streamRanging = flutterBeacon.ranging(regions).listen((RangingResult result) {
  // result contains a region and list of beacons found
  // list can be empty if no matching beacons were found in range
  print(result);
      //logtxt = logtxt + "result(" + result.toString() + ")...";
      if (result != null && mounted) {
        _regionBeacons[result.region] = result.beacons;
        _beacons.clear();
        _regionBeacons.values.forEach((list) {
        _beacons.addAll(list);
        });
        if (_beacons.length > 0) {
          logtxt = logtxt +
               "result(" +
                _beacons.length.toString() +
                ")" +
                result.toString() +
                "...";
          }

          _beacons.sort(_compareParameters);
          //_beacons.map((beacon) => beaconRxDb(beacon));
        });
        //_beacons.map((beacon) => beaconRxDb(beacon));
      }
});

// to stop ranging beacons
_streamRanging.cancel();
*/
}
