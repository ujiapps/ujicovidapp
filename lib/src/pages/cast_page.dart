// Librería de test para emisión de beacons
// Fusionada con beacon en beaconcast

import 'package:flutter/material.dart';
import 'dart:async';
import 'package:beacon_broadcast/beacon_broadcast.dart';

//void main() => runApp(CastApp());

class CastApp extends StatefulWidget {
  @override
  _CastAppState createState() => _CastAppState();
}

// https://openuuid.net/signin/
class _CastAppState extends State<CastApp> {
  static const UUID = '3A8B9673-BF9A-4676-ACAB-D9C4FB3E9727';
  static const MAJOR_ID = 74;
  static const MINOR_ID = 1001;
  static const TRANSMISSION_POWER = -59;
  static const IDENTIFIER = 'es.uji.covid.app';
  //static const LAYOUT = BeaconBroadcast.ALTBEACON_LAYOUT;
  static const LAYOUT = 'm:2-3=0215,i:4-19,i:20-21,i:22-23,p:24-24'; // iBeacon
  //static const MANUFACTURER_ID = 0x0118;
  static const MANUFACTURER_ID = 0x004c; // iBeacon:

  BeaconBroadcast beaconBroadcast = BeaconBroadcast();

  BeaconStatus _isTransmissionSupported;
  bool _isAdvertising = false;
  StreamSubscription<bool> _isAdvertisingSubscription;

  @override
  void initState() {
    super.initState();
    beaconBroadcast
        .checkTransmissionSupported()
        .then((isTransmissionSupported) {
      setState(() {
        _isTransmissionSupported = isTransmissionSupported;
      });
    });

    _isAdvertisingSubscription =
        beaconBroadcast.getAdvertisingStateChange().listen((isAdvertising) {
      setState(() {
        _isAdvertising = isAdvertising;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('UJI Beacon Broadcast'),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text('Tx habilitada',
                  style: Theme.of(context).textTheme.headline5),
              Text('$_isTransmissionSupported',
                  style: Theme.of(context).textTheme.subtitle1),
              Container(height: 16.0),
              Text('Beacon iniciado',
                  style: Theme.of(context).textTheme.headline5),
              Text('$_isAdvertising',
                  style: Theme.of(context).textTheme.subtitle1),
              Container(height: 16.0),
              Center(
                child: RaisedButton(
                  onPressed: () {
                    beaconBroadcast
                        .setUUID(UUID)
                        .setMajorId(MAJOR_ID)
                        .setMinorId(MINOR_ID)
                        .setTransmissionPower(TRANSMISSION_POWER)
                        .setIdentifier(IDENTIFIER)
                        .setLayout(LAYOUT)
                        .setManufacturerId(MANUFACTURER_ID)
                        .start();
                  },
                  child: Text('START'),
                ),
              ),
              Center(
                child: RaisedButton(
                  onPressed: () {
                    beaconBroadcast.stop();
                  },
                  child: Text('STOP'),
                ),
              ),
              Text('Beacon Data', style: Theme.of(context).textTheme.headline5),
              Text('UUID: $UUID'),
              Text('Major id: $MAJOR_ID'),
              Text('Minor id: $MINOR_ID'),
              Text('Tx Power: $TRANSMISSION_POWER'),
              Text('Identifier: $IDENTIFIER'),
              Text('Layout: $LAYOUT'),
              Text('Manufacturer Id: $MANUFACTURER_ID'),
            ],
          ),
        ),
      ),
    );
  }

  @override
  void dispose() {
    super.dispose();
    if (_isAdvertisingSubscription != null) {
      _isAdvertisingSubscription.cancel();
    }
  }
}
