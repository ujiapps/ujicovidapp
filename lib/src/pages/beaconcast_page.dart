// Librería de emisión y recepción simultánea de beacons
// Basada en beacon y cast.

import 'package:flutter/material.dart';
import 'dart:async';
import 'package:beacon_broadcast/beacon_broadcast.dart';

import 'dart:io';
//import 'dart:math';

import 'package:flutter/services.dart';
import 'package:flutter_beacon/flutter_beacon.dart';

//import 'package:ujicovidapp/src/services/sharedpref_serv.dart';
import 'package:ujicovidapp/src/lib.dart';
import 'package:ujicovidapp/src/services/sqlite_serv.dart';
import 'package:ujicovidapp/src/services/sharedpref_serv.dart';

//void main() => runApp(BeaconApp());

class BeaconCastApp extends StatefulWidget {
  @override
  _BeaconCastAppState createState() => _BeaconCastAppState();
}

class _BeaconCastAppState extends State<BeaconCastApp>
    with WidgetsBindingObserver {
  final StreamController<BluetoothState> streamController = StreamController();
  StreamSubscription<BluetoothState> _streamBluetooth;
  StreamSubscription<RangingResult> _streamRanging;
  final _regionBeacons = <Region, List<Beacon>>{};
  final _beacons = <Beacon>[];
  bool authorizationStatusOk = false;
  bool locationServiceEnabled = false;
  bool bluetoothEnabled = false;

  @override
  void initState() {
    WidgetsBinding.instance.addObserver(this);

    super.initState();

    listeningState();
  }

  listeningState() async {
    print('Listening to bluetooth state');
    _streamBluetooth = flutterBeacon
        .bluetoothStateChanged()
        .listen((BluetoothState state) async {
      print('BluetoothState = $state');
      streamController.add(state);

      switch (state) {
        case BluetoothState.stateOn:
          initScanBeacon();
          break;
        case BluetoothState.stateOff:
          await pauseScanBeacon();
          await checkAllRequirements();
          break;
      }
    });
  }

  checkAllRequirements() async {
    final bluetoothState = await flutterBeacon.bluetoothState;
    final bluetoothEnabled = bluetoothState == BluetoothState.stateOn;
    final authorizationStatus = await flutterBeacon.authorizationStatus;
    final authorizationStatusOk =
        authorizationStatus == AuthorizationStatus.allowed ||
            authorizationStatus == AuthorizationStatus.always;
    final locationServiceEnabled =
        await flutterBeacon.checkLocationServicesIfEnabled;

    setState(() {
      this.authorizationStatusOk = authorizationStatusOk;
      this.locationServiceEnabled = locationServiceEnabled;
      this.bluetoothEnabled = bluetoothEnabled;
    });
  }

  initScanBeacon() async {
    await flutterBeacon.initializeScanning;
    await checkAllRequirements();
    if (!authorizationStatusOk ||
        !locationServiceEnabled ||
        !bluetoothEnabled) {
      print('RETURNED, authorizationStatusOk=$authorizationStatusOk, '
          'locationServiceEnabled=$locationServiceEnabled, '
          'bluetoothEnabled=$bluetoothEnabled');
      return;
    }
    final regions = <Region>[
      Region(
        identifier: identifier,
        proximityUUID: uuid,
      ),
    ];

    if (_streamRanging != null) {
      if (_streamRanging.isPaused) {
        _streamRanging.resume();
        return;
      }
    }

    _streamRanging =
        flutterBeacon.ranging(regions).listen((RangingResult result) {
      print(result);
      //logtxt = logtxt + "result(" + result.toString() + ")...";
      if (result != null && mounted) {
        setState(() {
          _regionBeacons[result.region] = result.beacons;
          _beacons.clear();
          _regionBeacons.values.forEach((list) {
            _beacons.addAll(list);
          });
          if (_beacons.length > 0) {
            logtxt = logtxt +
                "result(" +
                _beacons.length.toString() +
                ")" +
                result.toString() +
                "...";
          }

          _beacons.sort(_compareParameters);
          //_beacons.map((beacon) => beaconRxDb(beacon));
          _beacons.forEach((beacon) => beaconRxDb(beacon));
        });
      }
    });
  }

  beaconRxDb(Beacon beacon) async {
    logtxt = logtxt + "Iniciando beaconRxDb...";
    DataRx datarx = DataRx(
        major: beacon.major,
        minor: beacon.minor,
        timer: (new DateTime.now()).millisecondsSinceEpoch,
        retry: 0,
        accur: beacon.accuracy,
        rssi: beacon.rssi);
    logtxt = logtxt + "Claseado. A checkRx...";
    await DBProvider.db.checkRx(datarx);
    logtxt = logtxt + "Chequeado. A return...";
    return true;
  }

  pauseScanBeacon() async {
    _streamRanging?.pause();
    if (_beacons.isNotEmpty) {
      setState(() {
        _beacons.clear();
      });
    }
  }

  int _compareParameters(Beacon a, Beacon b) {
    int compare = a.proximityUUID.compareTo(b.proximityUUID);
    if (compare == 0) {
      compare = a.major.compareTo(b.major);
    }
    if (compare == 0) {
      compare = a.minor.compareTo(b.minor);
    }
    return compare;
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) async {
    print('AppLifecycleState = $state');
    if (state == AppLifecycleState.resumed) {
      if (_streamBluetooth != null && _streamBluetooth.isPaused) {
        _streamBluetooth.resume();
      }
      await checkAllRequirements();
      if (authorizationStatusOk && locationServiceEnabled && bluetoothEnabled) {
        await initScanBeacon();
      } else {
        await pauseScanBeacon();
        await checkAllRequirements();
      }
    } else if (state == AppLifecycleState.paused) {
      _streamBluetooth?.pause();
    }
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    streamController?.close();
    _streamRanging?.cancel();
    _streamBluetooth?.cancel();
    flutterBeacon.close;

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('UJI Covid App Tx/Rx'),
          centerTitle: false,
          actions: <Widget>[
            if (!authorizationStatusOk)
              IconButton(
                  icon: Icon(Icons.portable_wifi_off),
                  color: Colors.red,
                  onPressed: () async {
                    await flutterBeacon.requestAuthorization;
                  }),
            if (!locationServiceEnabled)
              IconButton(
                  icon: Icon(Icons.location_off),
                  color: Colors.red,
                  onPressed: () async {
                    if (Platform.isAndroid) {
                      await flutterBeacon.openLocationSettings;
                    } else if (Platform.isIOS) {}
                  }),
            StreamBuilder<BluetoothState>(
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  final state = snapshot.data;

                  if (state == BluetoothState.stateOn) {
                    return IconButton(
                      icon: Icon(Icons.bluetooth_connected),
                      onPressed: () {},
                      color: Colors.lightBlueAccent,
                    );
                  }

                  if (state == BluetoothState.stateOff) {
                    return IconButton(
                      icon: Icon(Icons.bluetooth),
                      onPressed: () async {
                        if (Platform.isAndroid) {
                          try {
                            await flutterBeacon.openBluetoothSettings;
                          } on PlatformException catch (e) {
                            print(e);
                          }
                        } else if (Platform.isIOS) {}
                      },
                      color: Colors.red,
                    );
                  }

                  return IconButton(
                    icon: Icon(Icons.bluetooth_disabled),
                    onPressed: () {},
                    color: Colors.grey,
                  );
                }

                return SizedBox.shrink();
              },
              stream: streamController.stream,
              initialData: BluetoothState.stateUnknown,
            ),
          ],
        ),
        body: Column(
          children: <Widget>[
            CastBox(),
            Row(children: <Widget>[
              Expanded(child: Divider()),
              IconButton(
                icon: Icon(Icons.bluetooth_searching),
                color: Colors.grey,
                highlightColor: Colors.red,
                hoverColor: Colors.green,
                focusColor: Colors.purple,
                splashColor: Colors.yellow,
                disabledColor: Colors.amber,
                iconSize: 32,
                onPressed: () {
                  // stateScanBeacon ? pauseScanBeacon() : initScanBeacon();
                },
              ),
              Expanded(child: Divider()),
            ]),
            Container(
              //height: 256,
              child: _beacons == null || _beacons.isEmpty
                  ? Center(child: CircularProgressIndicator())
                  : Expanded(
                      child: ListView(
                        children: ListTile.divideTiles(
                            context: context,
                            tiles: _beacons.map((beacon) {
                              return ListTile(
                                title: Text(beacon.proximityUUID),
                                subtitle: new Row(
                                  mainAxisSize: MainAxisSize.max,
                                  children: <Widget>[
                                    Flexible(
                                        child: Text(
                                            'Major: ${beacon.major}\nMinor: ${beacon.minor}\nMAC: ${beacon.macAddress}',
                                            style: TextStyle(fontSize: 13.0)),
                                        // flex: 1,
                                        fit: FlexFit.tight),
                                    Flexible(
                                        child: Text(
                                            'Accuracy: ${beacon.accuracy}m\nRSSI: ${beacon.rssi}\nTxPower: ${beacon.txPower}',
                                            style: TextStyle(fontSize: 13.0)),
                                        // flex: 2,
                                        fit: FlexFit.tight)
                                  ],
                                ),
                              );
                            })).toList(),
                      ),
                    ),
            )
          ],
        ));
  }
}

class CastBox extends StatefulWidget {
  @override
  _CastBoxState createState() => _CastBoxState();
}

// https://openuuid.net/signin/
class _CastBoxState extends State<CastBox> {
  //static const UUID = '3A8B9673-BF9A-4676-ACAB-D9C4FB3E9727';
  //static const MAJOR_ID = 74;
  //static const MINOR_ID = 1001;
  //static const TRANSMISSION_POWER = -59;
  //static const IDENTIFIER = 'es.uji.covid.app';
  //static const LAYOUT = BeaconBroadcast.ALTBEACON_LAYOUT;
  static const LAYOUT =
      'm:2-3=0215,i:4-19,i:20-21,i:22-23,p:24-24'; // Formato iBeacon
  //static const MANUFACTURER_ID = 0x0118;
  static const MANUFACTURER_ID = 0x004c; // Formato iBeacon:

  BeaconBroadcast beaconBroadcast = BeaconBroadcast();

  BeaconStatus _isTransmissionSupported;
  bool _isAdvertising = false;
  StreamSubscription<bool> _isAdvertisingSubscription;

  //var randomId = Random();
  //int majorId;
  //int minorId;

  void beaconBCstart() async {
    final prefs = new SharedPrefServ();
    await prefs.initPrefs();
    await prefs.checkCastId();
    //await checkCastId();
    //final datatx = DataTx(major: majorid, minor: minorid, timer: lastidrenew);
    //await DBProvider.db.insTx(datatx);
    setState(() {
      beaconBroadcast
          .setUUID(uuid) // UUID
          .setMajorId(majorid) // MAJOR_ID
          .setMinorId(minorid) // MINOR_ID
          .setTransmissionPower(txpwr) // TRANSMISSION_POWER
          .setIdentifier(identifier) // IDENTIFIER
          .setLayout(LAYOUT)
          .setManufacturerId(MANUFACTURER_ID)
          .start();
    });
  }

  @override
  void initState() {
    super.initState();
    beaconBroadcast
        .checkTransmissionSupported()
        .then((isTransmissionSupported) {
      setState(() {
        _isTransmissionSupported = isTransmissionSupported;
      });
    });

    _isAdvertisingSubscription =
        beaconBroadcast.getAdvertisingStateChange().listen((isAdvertising) {
      setState(() {
        _isAdvertising = isAdvertising;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      /*
      appBar: AppBar(
        title: const Text('BeaconCast'),
      ),
      */
      height: 256,
      child: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text('TxReady: $_isTransmissionSupported',
                  style: Theme.of(context).textTheme.subtitle1),
              Container(height: 16.0),
              Text('TxON: $_isAdvertising',
                  style: Theme.of(context).textTheme.subtitle1),
              Container(height: 16.0),
              Row(
                children: <Widget>[
                  Expanded(
                    child: RaisedButton(
                      onPressed: beaconBCstart,
                      child: Text('START'),
                    ),
                  ),
                  Text(" "),
                  Expanded(
                    child: RaisedButton(
                      onPressed: () {
                        beaconBroadcast.stop();
                      },
                      child: Text('STOP'),
                    ),
                  ),
                ],
              ),
              Text('Rx Beacon Data',
                  style: Theme.of(context).textTheme.headline5),
              Text('UUID: $uuid'),
              //Text('Major id: $MAJOR_ID'),
              //Text('Minor id: $MINOR_ID'),
              Text('Major-Minor id: $majorid-$minorid'), // $MAJOR_ID-$MINOR_ID
              Text('Last Id Renew: ' +
                  new DateTime.fromMillisecondsSinceEpoch(lastidrenew)
                      .toString()),
              //Text('Tx Power: $TRANSMISSION_POWER'),
              Text('Identifier: $identifier'),
              Text('Layout: $LAYOUT'),
              //Text('Manufacturer Id: $MANUFACTURER_ID'),
              Text('Manufacturer Id: $MANUFACTURER_ID - Tx Power: $txpwr'),
            ],
          ),
        ),
      ),
    );
  }

  @override
  void dispose() {
    super.dispose();
    if (_isAdvertisingSubscription != null) {
      _isAdvertisingSubscription.cancel();
    }
  }
}
