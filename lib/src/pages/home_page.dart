// Pantalla inicial

import 'package:flutter/material.dart';
import 'dart:io';

class HomePage extends StatelessWidget {
  HomePage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          const RaisedButton(
            onPressed: null,
            child: Text('UJI COVID App', style: TextStyle(fontSize: 20)),
          ),
          const SizedBox(height: 30),
          RaisedButton(
            onPressed: () {
              Navigator.pushNamed(context, '/foreplug');
            },
            color: Colors.red,
            child: const Text('Foreground', style: TextStyle(fontSize: 20)),
          ),
          /*
          const SizedBox(height: 30),
          RaisedButton(
            onPressed: () {
              Navigator.pushNamed(context, '/cast'); // Prueba de emisión aislada
            },
            child: const Text('Cast', style: TextStyle(fontSize: 20)),
          ),
          const SizedBox(height: 30),
          RaisedButton(
            onPressed: () {
              Navigator.pushNamed(context, '/beacon'); // Prueba de lectura aislada
            },
            child: const Text('Beacon', style: TextStyle(fontSize: 20)),
            ),
          ),
          */
          const SizedBox(height: 30),
          RaisedButton(
            onPressed: () {
              Navigator.pushNamed(context, '/beaconcast');
            },
            color: Colors.green,
            child: const Text('BeaconCast', style: TextStyle(fontSize: 20)),
          ),
          const SizedBox(height: 30),
          RaisedButton(
            onPressed: () {
              Navigator.pushNamed(context, '/codes');
            },
            color: Colors.blue,
            child: const Text('Codes', style: TextStyle(fontSize: 20)),
          ),
          const SizedBox(height: 30),
          RaisedButton(
            onPressed: () {
              Navigator.pushNamed(context, '/admin');
            },
            color: Colors.red,
            child: const Text('Admin', style: TextStyle(fontSize: 20)),
          ),
          const SizedBox(height: 30),
          RaisedButton(
            onPressed: () => exit(0), // SystemNavigator.pop() || exit(0)
            color: Colors.black,
            textColor: Colors.white,
            child: const Text('EXIT', style: TextStyle(fontSize: 20)),
          ),
        ],
      ),
    );
  }
}
