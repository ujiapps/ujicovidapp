// Pantalla de test para desarrolladores

import 'package:flutter/material.dart';
import 'package:ujicovidapp/src/services/sqlite_serv.dart';
import 'package:ujicovidapp/src/lib.dart';
//import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_html/flutter_html.dart';

class AdminApp extends StatefulWidget {
  @override
  _AdminAppState createState() => _AdminAppState();
}

class _AdminAppState extends State<AdminApp> {
  String panelString = "Presionar botón...";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Admin Page")),
      body: Center(
          child: Column(
        children: <Widget>[
          Row(children: <Widget>[
            RaisedButton(
              onPressed: () async {
                String genTxStr = await getTxAll();
                setState(() {
                  panelString = genTxStr;
                });
              },
              child: Text('getTxAll'),
              textColor: Colors.white,
              color: Colors.green,
            ),
            RaisedButton(
              onPressed: () async {
                String genRxStr = await getRxAll();
                setState(() {
                  panelString = genRxStr;
                });
              },
              child: Text('getRx'),
              textColor: Colors.white,
              color: Colors.blue,
            ),
            RaisedButton(
              onPressed: () async {
                String conRxStr = await getConAll();
                setState(() {
                  panelString = conRxStr;
                });
              },
              child: Text('getCon'),
              textColor: Colors.white,
              color: Colors.blue,
            ),
          ]),
          Row(
            children: <Widget>[
              RaisedButton(
                onPressed: () async {
                  String conRxStr = await delDbAll();
                  setState(() {
                    panelString = conRxStr;
                  });
                },
                child: Text('delDb'),
                textColor: Colors.white,
                color: Colors.red,
              ),
              RaisedButton(
                  onPressed: () async {
                    setState(() {
                      panelString = logtxt.replaceAll('...', '<br>');
                    });
                  },
                  child: Text('LogTXT'),
                  textColor: Colors.white,
                  color: Colors.black),
            ],
          ),
          //Flexible(child: new Text(panelString))
          Html(data: panelString)
        ],
      )),
    );
  }
}

Future<String> getTxAll() async {
  print("Ejecutando getTxAll...");
  String genTxStr = await DBProvider.db.genCodeString();
  genTxStr = genTxStr.replaceAll('_', '<br>');
  return genTxStr;
}

Future<String> getRxAll() async {
  print("Ejecutando getRxAll...");
  String rxCodeStr = await DBProvider.db.rxCodeString();
  rxCodeStr = rxCodeStr.replaceAll('_', '<br>');
  return rxCodeStr;
}

Future<String> getConAll() async {
  print("Ejecutando getConAll...");
  String conCodeStr = await DBProvider.db.conCodeString();
  conCodeStr = conCodeStr.replaceAll('_', '<br>');
  return conCodeStr;
}

Future<String> delDbAll() async {
  print("Ejecutando getConAll...");
  await DBProvider.db.delDbAll();
  String delDbAllStr = "Borrado...";
  return delDbAllStr;
}
