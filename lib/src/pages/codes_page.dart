// Pantalla para el envío de código por positivo

import 'dart:async';
import 'dart:convert';
//import 'dart:io';
//import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
//import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
//import 'package:ujicovidapp/src/lib.dart';
import 'package:ujicovidapp/src/services/rest_serv.dart';
//import 'package:ujicovidapp/src/services/sqlite_serv.dart';

class JsonLockCode {
  int resLockCode;
  String msgLockCode;
  JsonLockCode({this.resLockCode, this.msgLockCode});
  //factory JsonLockCode.fromJson(Map<String, dynamic> parsedJson) {
  factory JsonLockCode.fromJson(String parsedJson) {
    //Map json = parsedJson;
    Map jsonResp = json.decode(parsedJson);
    return JsonLockCode(
        resLockCode: jsonResp['res'], msgLockCode: jsonResp['msg']);
  }
}

class CodesApp extends StatefulWidget {
  @override
  _CodesAppState createState() => _CodesAppState();
}

class _CodesAppState extends State<CodesApp> {
/*
  static BaseOptions options = BaseOptions(
      baseUrl: srvhost,
      responseType: ResponseType.plain,
      connectTimeout: 30000,
      receiveTimeout: 30000,
      validateStatus: (code) {
        if (code >= 200 && code <= 400) {
          // statusCode < 200 || statusCode > 400 || json == null
          return true;
        }
        return false;
      });
  static Dio dio = Dio(options);
*/
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  TextEditingController _lockcodeinputController = TextEditingController();

  bool _isLoading = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(title: Text("Envío de código por positivo")),
      body: Center(
        child: _isLoading
            ? CircularProgressIndicator()
            : Column(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(12.0),
                    child: TextField(
                      controller: _lockcodeinputController,
                      decoration: InputDecoration(
                        hintText: 'Código',
                      ),
                    ),
                  ),
                  RaisedButton(
                    child: Text("Enviar código"),
                    color: Colors.greenAccent,
                    onPressed: () async {
                      print("\nLockCodeLength:" +
                          _lockcodeinputController.text.length.toString() +
                          "\n");
                      if (_lockcodeinputController.text.length < 3) {
                        return showDialog(
                          context: context,
                          builder: (context) {
                            return AlertDialog(
                              content: Text("Introducir un código válido"),
                            );
                          },
                        );
                      } else {
                        setState(() => _isLoading = true);
                        String res = await RestServ.checkUnlockCode(
                            _lockcodeinputController.text);
                        setState(() => _isLoading = false);
                        if (res.isNotEmpty) {
                          JsonLockCode resCode = JsonLockCode.fromJson(res);
                          if ((resCode != null) && (resCode.resLockCode == 0)) {
                            final SharedPreferences prefs =
                                await SharedPreferences.getInstance();
                            await prefs.setString(
                                'lockcodeinput', _lockcodeinputController.text);
                            Navigator.of(context).push(MaterialPageRoute<Null>(
                                builder: (BuildContext context) {
                              return new RespScreen();
                            }));
                          } else {
                            return showDialog(
                              context: context,
                              builder: (context) {
                                return AlertDialog(
                                  content:
                                      Text("ERROR: " + resCode.msgLockCode),
                                );
                              },
                            );
                          }
                        } else {
                          Scaffold.of(context).showSnackBar(SnackBar(
                              content: Text("Sin conexión con el servidor")));
                        }
                      }
                    },
                  ),
                ],
              ),
      ),
    );
  }
}

class RespScreen extends StatefulWidget {
  @override
  _RespScreenState createState() => _RespScreenState();
}

class _RespScreenState extends State<RespScreen> {
  Future<void> _genCodeCall;

  @override
  void initState() {
    _genCodeCall = RestServ.sendGenCode();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Información por positivo"),
      ),
      body: FutureBuilder(
        future: _genCodeCall,
        builder: (context, snapshot) {
          print(snapshot);
          //if (snapshot.connectionState == ConnectionState.waiting)
          //if (!snapshot.hasData)
          if (snapshot.connectionState == ConnectionState.done &&
              snapshot.hasData &&
              snapshot.data != null)
            return Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text("Contactos enviados correctamente"),
                  Text("*Información para contagiados..."),
                ],
              ),
            );
          else if (snapshot.connectionState == ConnectionState.waiting)
            return Center(
              child: CircularProgressIndicator(),
            );
          else if (snapshot.hasError)
            return Center(
              child: Text("Error conectando..."),
            );
          else if (!snapshot.hasData || snapshot.data == null)
            return Center(
              child: Text("Respuesta vacía..."),
            );
          else
            return Center(
              child: Text("Error indeterminado..."),
            );
        },
      ),
    );
  }
}
