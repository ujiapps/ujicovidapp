// Definición de parámetros y librerías básicas de la app.

//import 'dart:math';
//import 'package:shared_preferences/shared_preferences.dart';
import 'package:ujicovidapp/src/services/rest_serv.dart';
import 'package:ujicovidapp/src/services/sqlite_serv.dart';

// INI configuración parámetros

final String uuid =
    '3A8B9673-BF9A-4676-ACAB-D9C4FB3E9727'; // UUID de emisión único por despliegue (Generado en // https://openuuid.net/signin/)
final String identifier = 'es.uji.covid.app'; // Identificador de la trama
final int txpwr = -59; // Potencia de Tx

final int paramIdRenewInt =
    30000; // Intervalo de renovación de códigos emitidos en milisec
final String appid = "123456789"; // Clave única para la comunicación con la API
final String srvhost = "https://www.uji.es"; // URL base del servidor
final String srvpath = "/api"; // Path del API en el servidor
final int paramTmpContactTimer =
    120000; // Intervalo en milisecs para ser considerado contacto de riesgo
final int paramTmpContactRetry =
    30; // Número de detecciones en el intervalo para ser considerado contacto de riesgo
final int paramContactOld =
    1296000000; // Tiempo en milisecs para mantener los códigos generados (1296000000=15 días)

// FIN configuración parámetros.

String logtxt = "Iniciando LogTXT...";

int majorid = 0;
int minorid = 0;
int lastidrenew = 0;

int currentTimeInSeconds() {
  var ms = (new DateTime.now()).millisecondsSinceEpoch;
  return (ms / 1000).round();
}

Future<bool> checkPosCodes() async {
  bool poscheck = false;
  String posCodesStr = await RestServ.getPosCodes();
  print("\nposCodesStr:" + posCodesStr + "\n");
  List posCodesLst = posCodesStr.split("_");
  List conCodesLst = await DBProvider.db.conCodeList();
  conCodesLst.forEach((element) {
    if (posCodesLst.contains(element)) {
      poscheck = true;
    }
  });
  return poscheck;
}

/*
Future<bool> checkCastId() async {
  final SharedPreferences prefs = await SharedPreferences.getInstance();
  if (prefs.containsKey('lastidrenew')) {
    lastidrenew = prefs.getInt('lastidrenew');
    //int nowtime = currentTimeInSeconds();
    int nowtime = (new DateTime.now()).millisecondsSinceEpoch.toInt();
    if (nowtime > (lastidrenew + (paramIdRenewInt))) {
      await setCastId();
    }
  } else {
    await setCastId();
  }
  return true;
}

Future<bool> setCastId() async {
  final SharedPreferences prefs = await SharedPreferences.getInstance();
  var randomId = Random();
  majorid = randomId.nextInt(65533) + 1; // 0 - 65535 intervalo
  minorid = randomId.nextInt(65533) + 1; // 0 & 65535 reservados
  //lastidrenew = currentTimeInSeconds();
  lastidrenew = (new DateTime.now()).millisecondsSinceEpoch.toInt();
  print("SetCastId: " +
      majorid.toString() +
      "-" +
      minorid.toString() +
      " (" +
      new DateTime.fromMillisecondsSinceEpoch(lastidrenew).toString() +
      ")");
  await prefs.setInt('majorid', majorid);
  await prefs.setInt('minorid', minorid);
  await prefs.setInt('lastidrenew', lastidrenew);
  DataTx datatx = DataTx(major: majorid, minor: minorid, timer: lastidrenew);
  await DBProvider.db.insTx(datatx);
  return true;
}
*/
