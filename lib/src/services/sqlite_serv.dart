// Funciones DB local.

//import 'dart:convert';

import 'dart:io';
import 'dart:async';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path_provider/path_provider.dart';

import 'package:ujicovidapp/src/models/data_model.dart';
export 'package:ujicovidapp/src/models/data_model.dart';

import 'package:ujicovidapp/src/lib.dart';

class DBProvider {
  static Database _database;
  static final DBProvider db = DBProvider._();

  DBProvider._();

  Future<Database> get database async {
    if (_database != null) return _database;

    _database = await initDB();
    return _database;
  }

  initDB() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();

    final path = join(documentsDirectory.path, 'Data.db');

    return await openDatabase(path, version: 1, onOpen: (db) {},
        onCreate: (Database db, int version) async {
      await db.execute('CREATE TABLE tx ('
          ' id INTEGER PRIMARY KEY AUTOINCREMENT,'
          ' major INTEGER,'
          ' minor INTEGER,'
          ' timer INTEGER'
          ')');
      await db.execute('CREATE TABLE rx ('
          ' id INTEGER PRIMARY KEY AUTOINCREMENT,'
          ' major INTEGER,'
          ' minor INTEGER,'
          ' timer INTEGER,'
          ' retry INTEGER,'
          ' accur REAL,'
          ' rssi INTEGER'
          ')');
      await db.execute('CREATE TABLE contact ('
          ' id INTEGER PRIMARY KEY AUTOINCREMENT,'
          ' major INTEGER,'
          ' minor INTEGER,'
          ' timer INTEGER,'
          ' retry INTEGER,'
          ' accur REAL,'
          ' rssi INTEGER'
          ')');
    });
  }

  insTx(DataTx insTx) async {
    final db = await database;
    insTx.id = await db.insert('tx', insTx.toJson());
    print('insTx id: ${insTx.id})');
    return insTx;
  }

  insRx(DataRx insRx) async {
    final db = await database;
    final res = await db.insert('rx', insRx.toJson());
    return res;
  }

  Future<int> delTxOld(int timer) async {
    final db = await database;
    final res = await db.delete('tx', where: 'timer < ?', whereArgs: [timer]);
    return res;
  }

  Future<int> delRxOld() async {
    final db = await database;
    final res = await db.delete('rx', where: 'timer < ?', whereArgs: [
      (new DateTime.now()).millisecondsSinceEpoch.toInt() - paramContactOld
    ]);
    return res;
  }

  Future<int> delRxAll() async {
    final db = await database;
    final res = await db.delete('rx', where: 'id > ?', whereArgs: [0]);
    return res;
  }

  Future<bool> delDbAll() async {
    final db = await database;
    await db.delete('rx', where: 'id > ?', whereArgs: [0]);
    await db.delete('tx', where: 'id > ?', whereArgs: [0]);
    await db.delete('contact', where: 'id > ?', whereArgs: [0]);
    return true;
  }

  checkRx(DataRx insRx) async {
    final db = await database;
    final oldRx = await db.query('rx',
        where: 'major = ? AND minor = ?',
        whereArgs: [insRx.major, insRx.minor],
        orderBy: 'id DESC',
        limit: 1);
    if (oldRx.isNotEmpty) {
      if (insRx.timer < oldRx[0]['timer'] + paramTmpContactTimer) {
        insRx.retry = oldRx[0]['retry'] + 1;
        if (insRx.accur < oldRx[0]['accur']) {
          insRx.accur = oldRx[0]['accur'];
        }
        if (insRx.rssi < oldRx[0]['rssi']) {
          insRx.rssi = oldRx[0]['rssi'];
        }
        if (insRx.retry >= paramTmpContactRetry) {
          await db.insert('contact', insRx.toJson());
          await db.delete('rx',
              where: 'major = ? AND minor = ?',
              whereArgs: [insRx.major, insRx.minor]);
        } else {
          await db.update('rx', insRx.toJson(),
              where: 'id = ?', whereArgs: [oldRx[0]['id']]);
        }
      } else {
        await db.delete('rx',
            where: 'major = ? AND minor = ?',
            whereArgs: [insRx.major, insRx.minor]);
        await db.insert('rx', insRx.toJson());
      }
    } else {
      await db.insert('rx', insRx.toJson());
    }

    //return res.isNotEmpty ? DataRx.fromJson(res.first) : null;
    return 0;
  }

  getRxAll() async {
    final db = await database;
    final res = await db.query('rx');
    List<DataRx> list =
        res.isNotEmpty ? res.map((c) => DataRx.fromJson(c)).toList() : [];
    return list;
  }

  genCodeString() async {
    final db = await database;
    final res = await db.query('tx');
    DataTx rowTemp;
    String genCodeStr = "0-65535-0";
    res.forEach((r) {
      rowTemp = DataTx.fromJson(r);
      genCodeStr = genCodeStr +
          "_" +
          rowTemp.major.toString() +
          "-" +
          rowTemp.minor.toString() +
          "-" +
          rowTemp.timer.toString();
    });
    genCodeStr = genCodeStr + "_0-65535-0";
    return genCodeStr;
  }

  rxCodeString() async {
    final db = await database;
    final res = await db.query('rx');
    DataRx rowTemp;
    String rxCodeStr = "0-65535-0-0-0-0";
    res.forEach((r) {
      rowTemp = DataRx.fromJson(r);
      rxCodeStr = rxCodeStr +
          "_" +
          rowTemp.major.toString() +
          "-" +
          rowTemp.minor.toString() +
          "-" +
          rowTemp.timer.toString() +
          "-" +
          rowTemp.retry.toString() +
          "-" +
          rowTemp.accur.toString() +
          "-" +
          rowTemp.rssi.toString();
    });
    rxCodeStr = rxCodeStr + "_0-65535-0-0-0-0";
    return rxCodeStr;
  }

  conCodeString() async {
    final db = await database;
    final res = await db.query('contact');
    DataRx rowTemp;
    String genCodeStr = "0-65535";
    res.forEach((r) {
      rowTemp = DataRx.fromJson(r);
      genCodeStr = genCodeStr +
          "_" +
          rowTemp.major.toString() +
          "-" +
          rowTemp.minor.toString();
    });
    genCodeStr = genCodeStr + "_0-65535";
    return genCodeStr;
  }

  conCodeList() async {
    final db = await database;
    final res = await db.query('contact');
    DataRx rowTemp;
    List<String> genCodeLst = List<String>();
    genCodeLst.add("0-65535");
    res.forEach((r) {
      rowTemp = DataRx.fromJson(r);
      genCodeLst.add(rowTemp.major.toString() + "-" + rowTemp.minor.toString());
    });
    genCodeLst.add("0-65535");
    return genCodeLst;
  }
}
