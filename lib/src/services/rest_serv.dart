// Funciones de tx/rx al API

import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:ujicovidapp/src/lib.dart';
import 'package:ujicovidapp/src/services/sqlite_serv.dart';

class RestServ {
  static Future<dynamic> getPosCodes() async {
    String url = srvhost + srvpath + '/gencode.php';
    Map bodyMap = {"appid": appid};
    // {"appid":"123456789"}
    final http.Response response = await http.post(
      url,
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(bodyMap),
    );

    final int statusCode = response.statusCode;
    if (statusCode < 200 || statusCode > 400 || json == null) {
      throw new Exception("Error al conectar.");
    } else {
      print("\n\nRESPONSE(" +
          statusCode.toString() +
          "):" +
          response.body +
          "\n\n");
      return response.body;
      //return JsonLockCode.fromJson(json.decode(response.body));
    }
  }

  static Future<dynamic> checkUnlockCode(String lockcodeinput) async {
    String url = srvhost + srvpath + '/lockcode.php';
    print("\nUnlockCodeURL:" + url + "\n");
    Map bodyMap = {"appid": appid, "lockcode": lockcodeinput};
    // {"appid":"123456789","lockcode":lockcodeinput}
    final http.Response response = await http.post(
      url,
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(bodyMap),
    );

    final int statusCode = response.statusCode;
    if (statusCode < 200 || statusCode > 400 || json == null) {
      throw new Exception("Error al conectar.");
    } else {
      print("\n\nRESPONSE(" +
          statusCode.toString() +
          "):" +
          response.body +
          "\n\n");
      return response.body;
      //return JsonLockCode.fromJson(json.decode(response.body));
    }
  }

  static Future<dynamic> sendGenCode() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    String lockcodeinput = prefs.getString('lockcodeinput');
    String gencodearray = await DBProvider.db.genCodeString();
    String url = srvhost + srvpath + '/gencode.php';
    Map bodyMap = {
      "appid": appid,
      "lockcode": lockcodeinput,
      "gencode": gencodearray
    };
    // {"appid":"123456789","lockcode":lockcodeinput}
    final http.Response response = await http.post(
      url,
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(bodyMap),
    );

    final int statusCode = response.statusCode;
    if (statusCode < 200 || statusCode > 400 || json == null) {
      throw new Exception("Error al conectar.");
    } else {
      print("\n\nRESPONSE(" +
          statusCode.toString() +
          "):" +
          response.body +
          "\n\n");
      return response.body;
      //return JsonLockCode.fromJson(json.decode(response.body));
    }
  }
}
