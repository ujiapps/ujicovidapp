// Funciones de almacenamiento local de variables

//import 'dart:io';
import 'dart:math';

import 'package:ujicovidapp/src/lib.dart';

import 'package:shared_preferences/shared_preferences.dart';

import 'package:ujicovidapp/src/services/sqlite_serv.dart';

class SharedPrefServ {
  static final SharedPrefServ _instance = new SharedPrefServ._internal();

  factory SharedPrefServ() {
    return _instance;
  }

  SharedPrefServ._internal();

  SharedPreferences _prefs;

  initPrefs() async {
    this._prefs = await SharedPreferences.getInstance();
  }

  checkCastId() async {
    if (_prefs.containsKey('lastidrenew')) {
      lastidrenew = this._prefs.getInt('lastidrenew');
      //int nowtime = currentTimeInSeconds();
      int nowtime = (new DateTime.now()).millisecondsSinceEpoch.toInt();
      if (nowtime > (lastidrenew + (paramIdRenewInt))) {
        await setCastId();
      }
    } else {
      await setCastId();
    }
    return true;
  }

  setCastId() async {
    var randomId = Random();
    majorid = randomId.nextInt(65533) + 1; // 0 - 65535 intervalo
    minorid = randomId.nextInt(65533) + 1; // 0 & 65535 reservados
    //lastidrenew = currentTimeInSeconds();
    lastidrenew = (new DateTime.now()).millisecondsSinceEpoch.toInt();
    print("SetCastId: " +
        majorid.toString() +
        "-" +
        minorid.toString() +
        " (" +
        new DateTime.fromMillisecondsSinceEpoch(lastidrenew).toString() +
        ")");
    _prefs.setInt('majorid', majorid);
    _prefs.setInt('minorid', minorid);
    _prefs.setInt('lastidrenew', lastidrenew);
    DataTx datatx = DataTx(major: majorid, minor: minorid, timer: lastidrenew);
    await DBProvider.db.insTx(datatx);
    return true;
  }

  get getMajorId {
    return _prefs.getInt('majorid') ?? 0;
  }

  get getMinorId {
    return _prefs.getInt('minorid') ?? 0;
  }

  get getLastIdRenew {
    return _prefs.getInt('lastidrenew') ?? 0;
  }

  set setMajorId(int value) {
    _prefs.setInt('majorid', value);
  }

  set setMinorId(int value) {
    _prefs.setInt('minorid', value);
  }

  set setLastIdRenew(int value) {
    _prefs.setInt('lastidrenew', value);
  }

  bool containsKey(String value) {
    bool resp = _prefs.containsKey(value);
    return resp;
  }
}
